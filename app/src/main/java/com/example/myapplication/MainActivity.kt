package com.example.myapplication

import android.content.SharedPreferences
import android.location.Address
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        read()

    }

    private fun init() {
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)
        SaveButton.setOnClickListener {
            save()
        }


    }



    private fun save() {
        val email = Email.text.toString()
        val firstName = FirstName.text.toString()
        val lastName = LastName.text.toString()
        val age = Age1.text.toString().toInt()
        val address = Address.text.toString()

        val editor = sharedPreferences.edit()
        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.toString().isNotEmpty() && address.isNotEmpty()){
            editor.putString("email", email)
            editor.putString("firstname", firstName)
            editor.putString("lastname", lastName)
            editor.putString("age", age.toString())
            editor.putString("address", address)
            editor.apply()
        }else{
            Toast.makeText(this, "Fill All field", Toast.LENGTH_SHORT).show()
        }


    }

    private fun read() {
        val email = sharedPreferences.getString("email", "")
        val firstname = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val age = sharedPreferences.getString("age", "")
        val address = sharedPreferences.getString("address", "")
        if (email!!.isEmpty()){
            Email.setText("")
            FirstName.setText("")
            LastName.setText("")
            Age1.setText("")
            Address.setText("")
        }else{
            Email.setText(email)
            FirstName.setText(firstname)
            LastName.setText(lastname)
            Age1.setText(age)
            Address.setText(address)
        }

    }


}